package com.test;

import static com.main.utilitaires.Constante.*;
import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.main.domaine.Requete;
import com.main.domaine.categorie.Categorie;
import com.main.domaine.Technicien;


public class TestTechLogin {
	private Technicien tech;
	private String prenom;
	private String nom;
	private String UserName;
	private String motDePasse;
	private String role;
	private String modificator;
	private Requete requeteComplet;
	

	

	@Before
	public void setUp() throws Exception {
		prenom = "Bob";
		nom = "Test";
		UserName = "David";
		motDePasse = "test";
		role = "testeur";
		modificator= "Testing";
		tech = new Technicien(prenom, nom , UserName, motDePasse);
		requeteComplet = new Requete("bob", "bobAimeLeBacon", tech, Categorie.create(CATEGORIE_POSTE_DE_TRAVAIL));

	}

	@After
	public void exterminate() {
		tech = null;
	}

	@Test
	public void testGetRole() {
		
		String role = tech.getRole();
		assertEquals(role, "technicien");
	}
}
