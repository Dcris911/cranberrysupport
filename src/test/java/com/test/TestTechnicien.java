package com.test;

import static com.main.utilitaires.Constante.CATEGORIE_POSTE_DE_TRAVAIL;
import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.main.domaine.Requete;
import com.main.domaine.Statut.FactoryStatut;
import com.main.domaine.Statut.Statut;
import com.main.domaine.categorie.Categorie;
import com.main.domaine.Technicien;

import static com.main.utilitaires.Constante.*;

public class TestTechnicien {

	private Technicien tech;
	private String prenom;
	private String nom;
	private String UserName;
	private String motDePasse;
	private Requete requeteComplet;
	private Requete requeteTraitement;
	private Requete requeteOuvert;
	private Requete requeteAbandonner;

	@Before
	public void setUp() throws Exception {
		prenom = "Bob";
		nom = "Test";
		UserName = "David";
		motDePasse = "test";
		tech = new Technicien(prenom, nom , UserName, motDePasse);
		requeteComplet = new Requete("bob", "bobAimeLeBacon", tech, Categorie.create(CATEGORIE_POSTE_DE_TRAVAIL));
		requeteComplet.setStatut(FactoryStatut.create(STATUT_FINAL_SUCCESS));
		requeteTraitement = new Requete("bob", "bobAimeLeBacon", tech, Categorie.create(CATEGORIE_POSTE_DE_TRAVAIL));
		requeteTraitement.setStatut(FactoryStatut.create(STATUT_EN_TRAITEMENT));
		requeteOuvert = new Requete("bob", "bobAimeLeBacon", tech, Categorie.create(CATEGORIE_POSTE_DE_TRAVAIL));
		requeteOuvert.setStatut(FactoryStatut.create(STATUT_OUVERT));
		requeteAbandonner = new Requete("bob", "bobAimeLeBacon", tech, Categorie.create(CATEGORIE_POSTE_DE_TRAVAIL));
		requeteAbandonner.setStatut(FactoryStatut.create(STATUT_FINAL_ABANDON));
	}

	@After
	public void exterminate() {
		tech = null;
		prenom = null;
		nom = null;
		UserName = null;
		motDePasse = null;
		requeteComplet= null;
		requeteTraitement = null;
		requeteOuvert = null;
		requeteAbandonner =null;
	}

	/**
	 * WTF Pourquoi la Réponse est hard coder!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 */
	@Test
	public void testGetRole() {
		
		String role = tech.getRole();
		assertEquals(role, "technicien");
	}
	
	@Test
	public void testAjouterRequeteAssigneeEtGetListeRequeteAvecVerificationListeEnCours() {
		tech.ajouterRequeteAssignee(requeteComplet);
		assertEquals(requeteComplet, tech.getListeRequetes().get(0));
		//assertEquals(requeteComplet, tech.data.getListeRequetesEnCours().get(0));	
	}
	
	@Test
	public void testAjoutListRequetesFinies() {
		tech.ajoutListRequetesFinies(requeteComplet);
		//assertEquals(requeteComplet, tech.data.getListeRequetesFinies().get(0));
		//assertTrue(tech.data.getListeRequetesEnCours().isEmpty());	
	}
	
	@Test
	public void testAjoutRequete() {
		tech.ajoutRequete(requeteComplet);
		assertEquals(requeteComplet, tech.getListeRequetes().get(0));
		assertEquals(requeteComplet, tech.getListPerso().get(0));	
	}
	@Test
	public void TestGetListStatut() {
		tech.ajoutRequete(requeteComplet);
		tech.ajoutRequete(requeteAbandonner);
		tech.ajoutRequete(requeteOuvert);
		tech.ajoutRequete(requeteTraitement);
		List<Requete> list = tech.getListStatut(FactoryStatut.create(STATUT_OUVERT));
		assertEquals(requeteOuvert.getStatut(), list.get(0).getStatut());
		assertNotEquals(requeteTraitement, list.get(0));	
	}
	
	@Test
	public void TestGetRequeteParStatut() {
		tech.ajoutRequete(requeteComplet);
		tech.ajoutRequete(requeteAbandonner);
		tech.ajoutRequete(requeteOuvert);
		tech.ajoutRequete(requeteTraitement);
		String rapport = tech.getRequeteParStatut();
		assertEquals("Statut ouvert: 1\nStatut En traitement: 1\nStatut succès: 1\nStatut abandonnée: 1\n" ,rapport);
	}
	
}
