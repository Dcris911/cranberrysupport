package com.test.database;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.main.database.BanqueUtilisateurs;
import com.main.database.RepoUtilisateur;
import com.main.database.fichiertxt.BanqueRequetesTxt;
import com.main.database.fichiertxt.BanqueUtilisateursTxt;
import com.main.domaine.Client;
import com.main.domaine.Requete;
import com.main.domaine.Statut.FactoryStatut;
import com.main.domaine.categorie.Categorie;

import static com.main.utilitaires.Constante.*;

public class TestBanqueRequetes {

	private BanqueRequetesTxt banqueRequetes;
	
	private Client utilisateur;
	
	
	@Before
	public void setUp() throws Exception {
		banqueRequetes = new BanqueRequetesTxt();
		utilisateur = new Client("bob", "", "bob", "");
		
	}
	
	
	@Test
	public void testGetNo() throws FileNotFoundException, IOException {
		assertEquals(-1, (int)banqueRequetes.getNo());
	}

	@Test
	public void testIncrementeNo() throws FileNotFoundException, IOException {
		int noActuelle = banqueRequetes.getNo();
		assertEquals((int) noActuelle + 1, (int) banqueRequetes.incrementeNo());
	}
	
	@Test
	public void testNewRequete() throws FileNotFoundException, IOException {
		int nombreRequete = banqueRequetes.getListRequetes().size();
		banqueRequetes.newRequete("asd", "asd", utilisateur, Categorie.create(CATEGORIE_AUTRE));
		assertEquals(nombreRequete + 1, banqueRequetes.getListRequetes().size());
	}

	@Test
	public void testReturnLast() throws FileNotFoundException, IOException {
		banqueRequetes.newRequete("premier", "asd", utilisateur, Categorie.create(CATEGORIE_AUTRE));
		banqueRequetes.newRequete("deuxieme", "asd", utilisateur, Categorie.create(CATEGORIE_AUTRE));
		banqueRequetes.newRequete("troisieme", "asd", utilisateur, Categorie.create(CATEGORIE_AUTRE));
		assertEquals("troisieme", banqueRequetes.returnLast().getSujet());
		banqueRequetes.newRequete("quatrieme", "asd", utilisateur, Categorie.create(CATEGORIE_AUTRE));
		assertEquals("quatrieme", banqueRequetes.returnLast().getSujet());
	}

	@Test
	public void testGetListRequetesByStatut() throws FileNotFoundException, IOException {
		banqueRequetes.newRequete("asd", "asd", utilisateur, Categorie.create(CATEGORIE_AUTRE));
		banqueRequetes.newRequete("asd", "asd", utilisateur, Categorie.create(CATEGORIE_AUTRE));
		banqueRequetes.newRequete("asd", "asd", utilisateur, Categorie.create(CATEGORIE_AUTRE));
		banqueRequetes.newRequete("asd", "asd", utilisateur, Categorie.create(CATEGORIE_AUTRE));
		assertEquals(4, banqueRequetes.getListRequetesByStatut(FactoryStatut.create(STATUT_OUVERT)).size());
		Requete requete = banqueRequetes.returnLast();
		requete.setStatut(FactoryStatut.create(STATUT_EN_TRAITEMENT));
		assertEquals(1, banqueRequetes.getListRequetesByStatut(FactoryStatut.create(STATUT_EN_TRAITEMENT)).size());
		assertEquals(3, banqueRequetes.getListRequetesByStatut(FactoryStatut.create(STATUT_OUVERT)).size());
	}	
}
