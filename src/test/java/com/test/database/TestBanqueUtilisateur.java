package com.test.database;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.main.database.fichiertxt.BanqueUtilisateursTxt;
import com.main.domaine.Client;
import com.main.domaine.Technicien;

import static com.main.utilitaires.Constante.*;

public class TestBanqueUtilisateur {

	private BanqueUtilisateursTxt banqueUtilisateurs;
	
	@Before
	public void setUp() throws Exception {
		banqueUtilisateurs = new BanqueUtilisateursTxt();	
		banqueUtilisateurs.testMode();
	}
	@After
	public void exterminate() throws Exception {
		banqueUtilisateurs.dropTable();
	}

	@Test
	public void testGetListUtilisateur() {
		assertTrue(banqueUtilisateurs.getListUtilisateurs().isEmpty());
	}
	
	@Test
	public void testAddUtilisateurClient() {
		assertTrue(banqueUtilisateurs.getListUtilisateurs().isEmpty());
		assertTrue(banqueUtilisateurs.addUtilisateur("", "a", "", "", ROLE_CLIENT));
		assertEquals(1, banqueUtilisateurs.getListUtilisateurs().size());
	}
	
	@Test
	public void testAddUtilisateurTech() {
		assertTrue(banqueUtilisateurs.getListUtilisateurs().isEmpty());
		assertTrue(banqueUtilisateurs.addUtilisateur("", "a", "", "", ROLE_TECHNICIEN));
		assertEquals(1, banqueUtilisateurs.getListUtilisateurs().size());
	}
	
	@Test
	public void testAddUtilisateurExisteDejaMemeNom() {
		assertTrue(banqueUtilisateurs.getListUtilisateurs().isEmpty());
		assertTrue(banqueUtilisateurs.addUtilisateur("", "a", "a", "", ROLE_TECHNICIEN));
		assertFalse(banqueUtilisateurs.addUtilisateur("", "a", "b", "", ROLE_TECHNICIEN));
		assertEquals(1, banqueUtilisateurs.getListUtilisateurs().size());
	}
	
	@Test
	public void testAddUtilisateurExisteDejaMemeNomUtil() {
		assertTrue(banqueUtilisateurs.getListUtilisateurs().isEmpty());
		assertTrue(banqueUtilisateurs.addUtilisateur("", "a", "a", "", ROLE_TECHNICIEN));
		assertFalse(banqueUtilisateurs.addUtilisateur("", "b", "a", "", ROLE_TECHNICIEN));
		assertEquals(1, banqueUtilisateurs.getListUtilisateurs().size());
	}
	
	@Test
	public void testAddUtilisateurPasBonRole() {
		assertFalse(banqueUtilisateurs.addUtilisateur("", "a", "", "", "pasUnRole"));
	}
	
	@Test
	public void testGetListUtilisateurByRole() {
		addTechnicienEtClient();
		assertEquals(2 ,banqueUtilisateurs.getListUtilisateursByRole(ROLE_TECHNICIEN).size());
	}

	@Test
	public void testGetListUtilisateurByRoleMauvaisRole() {
		addTechnicienEtClient();
		assertTrue(banqueUtilisateurs.getListUtilisateursByRole("pasUnRole").isEmpty());
	}

	@Test
	public void testChercherUtilisateurClient() {
		addTechnicienEtClient();
		assertNotNull(banqueUtilisateurs.chercherUtilisateur(new Client("", "c", "", "")));
	}

	@Test
	public void testChercherUtilisateurTechnicien() {
		addTechnicienEtClient();
		assertNotNull(banqueUtilisateurs.chercherUtilisateur(new Technicien("", "b", "", "")));
	}

	@Test
	public void testChercherUtilisateurExistePas() {
		addTechnicienEtClient();
		assertNull(banqueUtilisateurs.chercherUtilisateur(new Client("", "existePas", "", "")));
	}

	@Test
	public void testChercherUtilisateurParNomUtil() {
		addTechnicienEtClient();
		assertNotNull(banqueUtilisateurs.chercherUtilisateurParNomUtil("a"));
		assertTrue(banqueUtilisateurs.chercherUtilisateurParNomUtil("a") instanceof Technicien);
	}

	@Test
	public void testChercherUtilisateurParNomUtilExistePas() {
		addTechnicienEtClient();
		assertNull(banqueUtilisateurs.chercherUtilisateurParNomUtil(""));
	}
	
	private void addTechnicienEtClient() {
		assertTrue(banqueUtilisateurs.getListUtilisateursByRole(ROLE_TECHNICIEN).isEmpty());
		assertTrue(banqueUtilisateurs.addUtilisateur("", "a", "a", "", ROLE_TECHNICIEN));
		assertTrue(banqueUtilisateurs.addUtilisateur("", "b", "b", "", ROLE_TECHNICIEN));
		assertTrue(banqueUtilisateurs.addUtilisateur("", "c", "c", "", ROLE_CLIENT));
	}
}
