package com.test;

import static com.main.utilitaires.Constante.*;
import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.main.domaine.Client;
import com.main.domaine.Commentaire;
import com.main.domaine.Requete;
import com.main.domaine.Technicien;
import com.main.domaine.Statut.FactoryStatut;
import com.main.domaine.Statut.Statut;
import com.main.domaine.categorie.Categorie;

import static com.main.utilitaires.Constante.*;

@SuppressWarnings("serial")
public class TestRequete {
	
	private Requete requeteSimple;
	private Requete requeteComplet;
	
	private Client client;
	private Technicien technicien;
	
	@Rule
	public TemporaryFolder testFolder = new TemporaryFolder();
	
	@Before
	public void setUp() throws Exception {
		client = new Client("", "", "bobinette", "xox");
		technicien = new Technicien("bob", "bobino", "bobino", "oxo");
		requeteSimple = new Requete("bob", "bobAimeLeFromage", client, Categorie.create(CATEGORIE_COMPTE_USAGER));
		requeteComplet = new Requete("bob", "bobAimeLeBacon", technicien, Categorie.create(CATEGORIE_POSTE_DE_TRAVAIL));
	}

	@Test
	public void testGetSujet() {
		assertEquals("bob", requeteSimple.getSujet());
		assertEquals("bob", requeteComplet.getSujet());
	}

	@Test
	public void testSetSujet() {
		requeteSimple.setSujet("pasBob");
		requeteComplet.setSujet("pasBobo");

		assertEquals("pasBob", requeteSimple.getSujet());
		assertEquals("pasBobo", requeteComplet.getSujet());
	}

	@Test
	public void testGetDescription() {
		assertEquals("bobAimeLeFromage", requeteSimple.getDescription());
		assertEquals("bobAimeLeBacon", requeteComplet.getDescription());
	}
	
	@Test
	public void testSetDescription() {
		requeteSimple.setDescription("bobNAimePlusLeFromage");
		requeteComplet.setDescription("bobNAimePlusLeBacon");

		assertEquals("bobNAimePlusLeFromage", requeteSimple.getDescription());
		assertEquals("bobNAimePlusLeBacon", requeteComplet.getDescription());
	}

	@Test
	public void testGetFichier() {
		assertEquals(null, requeteSimple.getFichier());
		assertEquals(null, requeteComplet.getFichier());
	}
	
	@Test
	public void testSetFichierFichierExiste() throws IOException {
		testFolder.newFile("dummyFile");
		testFolder.newFile("dummyFileComplet");
		
		requeteSimple.setFichier(new File("dummyFile"));
		requeteComplet.setFichier(new File("dummyFileComplet"));
		
		assertEquals(new File("dummyFile"), requeteSimple.getFichier());
		assertEquals(new File("dummyFileComplet"), requeteComplet.getFichier());
	}	
	
	@Test
	public void testSetFichierFichierExistePas() {
		requeteSimple.setFichier(new File("existePas"));
		requeteComplet.setFichier(new File("existePas"));
		
		assertEquals(new File("existePas"), requeteSimple.getFichier());
		assertEquals(new File("existePas"), requeteComplet.getFichier());
	}
	
	@Ignore
	@Test
	public void testGetNumero() {
		assertEquals(21, (int)requeteSimple.getNumero());
		assertEquals(22, (int)requeteComplet.getNumero());
	}
	
	@Test
	public void testGetClient() {
		assertEquals(client, requeteSimple.getClient());
		assertEquals(technicien, requeteComplet.getClient());
	}
	
	@Test
	public void testGetTech() {
		assertEquals(null, requeteSimple.getTech());
		assertEquals(technicien, requeteComplet.getTech());
	}
	
	@Test
	public void testSetTech() {
		requeteSimple.setTech(technicien);
		requeteComplet.setTech(technicien);
		assertEquals(technicien, requeteSimple.getTech());
		assertEquals(technicien, requeteComplet.getTech());
	}
	
	@Test
	public void testGetStatus() {
		assertEquals(STATUT_OUVERT, requeteSimple.getStatut().getStatut());
		assertEquals(STATUT_OUVERT, requeteComplet.getStatut().getStatut());
	}
	
	@Test
	public void testSetStatus() {
		requeteSimple.setStatut(FactoryStatut.create(STATUT_FINAL_SUCCESS));
		requeteComplet.setStatut(FactoryStatut.create(STATUT_EN_TRAITEMENT));
		assertEquals(STATUT_FINAL_SUCCESS, requeteSimple.getStatut().getStatut());
		assertEquals(STATUT_EN_TRAITEMENT, requeteComplet.getStatut().getStatut());
	}
	
	@Test
	public void testGetCategorie() {
		assertEquals(Categorie.create(CATEGORIE_COMPTE_USAGER), requeteSimple.getCategorie());
		assertEquals(Categorie.create(CATEGORIE_POSTE_DE_TRAVAIL), requeteComplet.getCategorie());
	}
	
	@Test
	public void testSetCategorie() {
		requeteSimple.setCategorie(Categorie.create(CATEGORIE_SERVEUR));
		requeteComplet.setCategorie(Categorie.create(CATEGORIE_SERVEUR));
		
		assertEquals(Categorie.create(CATEGORIE_SERVEUR), requeteSimple.getCategorie());
		assertEquals(Categorie.create(CATEGORIE_SERVEUR), requeteComplet.getCategorie());
	}
	
	@Test
	public void testGetCommentaire() {
		assertEquals(new ArrayList<>(), requeteSimple.getComments());
		assertEquals(new ArrayList<>(), requeteComplet.getComments());
	}
	
	@Test
	public void testFinaliser() {
		//assertEquals(0, technicien.data.getListeRequetesFinies().size());
		requeteComplet.finaliser(FactoryStatut.create(STATUT_FINAL_SUCCESS));
		//assertEquals(1, technicien.data.getListeRequetesFinies().size());
	}
	
	@Test
	public void testAddCommentaire() {
		requeteSimple.addCommentaire("Bob", client);
		requeteComplet.addCommentaire("Boby", technicien);

		Commentaire commentaire1 = new Commentaire("Bob", client);
		Commentaire commentaire2 = new Commentaire("Boby", technicien);
		
		assertEquals(new ArrayList<Commentaire>() {{add(commentaire1);}}, requeteSimple.getComments());
		assertEquals(new ArrayList<Commentaire>() {{add(commentaire2);}}, requeteComplet.getComments());
	}
 
	@Test
	public void testUploadFichier() {
		requeteComplet.uploadFichier();
	}
}
