package com.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.main.domaine.Statut.FactoryStatut;
import com.main.domaine.Statut.Statut;

import static com.main.utilitaires.Constante.*;

public class TestEnumStatut {
	
	@Test
	public void testStatutGetValueString() {
		Statut statut = FactoryStatut.create(STATUT_OUVERT);
		assertEquals("ouvert", statut.getStatut());
	}

	@Test
	public void testStatutFromStringOuvert() {
	    Statut statut = FactoryStatut.create(STATUT_OUVERT);
		assertEquals(STATUT_OUVERT, statut.getStatut());
	}

	@Test
	public void testStatutFromStringEnTraitement() {
	    Statut statut = FactoryStatut.create(STATUT_EN_TRAITEMENT);
		assertEquals(STATUT_EN_TRAITEMENT, statut.getStatut());
	}

	@Test
	public void testStatutFromStringFinalAbandon() {
	    Statut statut = FactoryStatut.create(STATUT_FINAL_ABANDON);
		assertEquals(STATUT_FINAL_ABANDON, statut.getStatut());
	}
	
	@Test
	public void testStatutFromStringFinalSuccess() {
	    Statut statut = FactoryStatut.create(STATUT_FINAL_SUCCESS);
		assertEquals(STATUT_FINAL_SUCCESS, statut.getStatut());
	}

	@Test (expected = NullPointerException.class)
	public void testStatutFromStringNull() {
		assertEquals(null, FactoryStatut.fromString("Bob"));
	}

}
