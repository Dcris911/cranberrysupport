package com.test;

import static com.main.utilitaires.Constante.*;
import static org.junit.Assert.*;

import org.junit.Test;

import com.main.domaine.categorie.CategorieInterface;
import com.main.domaine.categorie.Categorie;

public class TestCategorie {

	@Test
	public void testCategorieGetValueString() {
		CategorieInterface categorie = Categorie.create(CATEGORIE_COMPTE_USAGER);
		assertEquals("Compte usager", categorie.getValue());
	}

	@Test
	public void testCategorieFromStringOuvertPosteDeTravail() {
		assertEquals(Categorie.create(CATEGORIE_POSTE_DE_TRAVAIL).getValue(),
				Categorie.fromString("Poste de travail"));
	}

	@Test
	public void testCategorieFromStringOuvertServeur() {
		assertEquals(Categorie.create(CATEGORIE_SERVEUR).getValue(),
				Categorie.fromString("Serveur"));
	}

	@Test
	public void testCategorieFromStringOuvertServiceWeb() {
		assertEquals(Categorie.create(CATEGORIE_SERVICE_WEB).getValue(),
				Categorie.fromString("Service web"));
	}

	@Test
	public void testCategorieFromStringOuvertCompteUsager() {
		assertEquals(Categorie.create(CATEGORIE_COMPTE_USAGER).getValue(),
				Categorie.fromString("Compte usager"));
	}

	@Test
	public void testCategorieFromStringOuvertAutre() {
		assertEquals(Categorie.create(CATEGORIE_AUTRE).getValue(),
				Categorie.fromString("Autre"));
	}

	@Test(expected = NullPointerException.class)
	public void testCategorieFromStringOuvert() {
		Categorie.fromString("Bob");
	}
}
