package com.test;

import static org.junit.Assert.*;
import static com.main.utilitaires.Constante.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.main.domaine.Client;
import com.main.domaine.Requete;
import com.main.domaine.Statut.FactoryStatut;
import com.main.domaine.Statut.Statut;
import com.main.domaine.categorie.Categorie;

@SuppressWarnings("serial")
public class TestClient {

	private Client client;
	private Client client2;
	
	@Before
	public void setUp() throws Exception {
		client = new Client("prenom", "lenom", "nomUtilisateur", "mdp");
		client2 = new Client("", "", "", "");
	}

	/** Test de la classe utilisateur */
	
	@Test
	public void testUtilisateurFetchInfos() {
		assertEquals("lenom", client.fetchInfos().get(0));
	}
	
	@Test
	public void testUtilisateurLoginFonctionne() {
		assertTrue(client.login("nomUtilisateur", "mdp"));
	}
	
	@Test
	public void testUtilisateurLoginMauvaiseInfo() {
		assertFalse(client.login("nom", "mdp"));
	}
	
	/** Test Classe Client */
	
	@Test
	public void testGetRole() {
		assertEquals("client", client.getRole());
	}
	
	@Test
	public void testAjoutRequeteEtGetListStatut() throws FileNotFoundException, IOException {
		assertTrue(client.getListeRequetes().isEmpty());

		Requete requete2 = new Requete("sujet", "description", client, Categorie.create(CATEGORIE_COMPTE_USAGER));
		client.ajoutRequete(requete2);
		
		assertEquals(1, client.getListeRequetes().size());
		assertEquals(new ArrayList<Requete>() {{add(requete2);}}, client.getListeRequetes());
	}
	
	@Test
	public void testAjoutRequeteMauvaisClientEtGetListStatut() throws FileNotFoundException, IOException {
		assertTrue(client.getListeRequetes().isEmpty());
		
		Requete requete1 = new Requete("sujet", "description", client, Categorie.create(CATEGORIE_COMPTE_USAGER));
		client.ajoutRequete(requete1);
		
		assertEquals(1, client.getListeRequetes().size()); 
		assertEquals(new ArrayList<Requete>() {{add(requete1);}}, client.getListeRequetes());
	}
	
	@Test
	public void testTrouverRequeteParNumeroBonNumero() throws FileNotFoundException, IOException{
		Requete requete1 = new Requete("sujet", "description", client2, Categorie.create(CATEGORIE_COMPTE_USAGER));
		client.ajoutRequete(requete1);
		assertEquals(requete1.getSujet(), 
			client.trouverRequeteParNumero(requete1.getNumero()).getSujet());
	}
	
	@Test(expected = NullPointerException.class)
	public void testTrouverRequeteParNumeroMauvaisNumero() throws FileNotFoundException, IOException{
		Requete requete1 = new Requete("sujet", "description", client, Categorie.create(CATEGORIE_COMPTE_USAGER));
		client.ajoutRequete(requete1);
		client.trouverRequeteParNumero(requete1.getNumero() + 1000).getSujet();
	}
	
	@Test
	public void testTrouverRequeteParSujetBonSujet() throws FileNotFoundException, IOException{
		Requete requete1 = new Requete("sujetDeLaRequete", "description", 
				client, Categorie.create(CATEGORIE_COMPTE_USAGER));
		client.ajoutRequete(requete1);
		requete1 = new Requete("sujetDeLaRequete", "description", 
				client, Categorie.create(CATEGORIE_COMPTE_USAGER));
		client.ajoutRequete(requete1);
		assertEquals(2, 
				client.trouverRequeteParSujet("sujetDeLaRequete").size());
	}
	
	@Test
	public void testTrouverRequeteParSujetMauvaisSujet() throws FileNotFoundException, IOException{
		Requete requete1 = new Requete("sujetDeLaRequete", "description", 
				client, Categorie.create(CATEGORIE_COMPTE_USAGER));
		client.ajoutRequete(requete1);
		requete1 = new Requete("sujetDeLaRequete", "description", 
				client, Categorie.create(CATEGORIE_COMPTE_USAGER));
		client.ajoutRequete(requete1);
		assertEquals(0, 
				client.trouverRequeteParSujet("mauvaisSujet").size());
	}
	
	@Test
	public void testGetListeRequeteEmpty(){
		assertTrue(client.getListeRequetes().isEmpty());
	}
	
	@Test
	public void testGetListeRequeteNonEmpty() throws FileNotFoundException, IOException{
		Requete requete1 = new Requete("sujet", "description", client, Categorie.create(CATEGORIE_COMPTE_USAGER));
		client.ajoutRequete(requete1);
		
		assertEquals(1, client.getListeRequetes().size());
	}
	
	@Test(expected = UnsupportedOperationException.class)
	public void testGetListePerso(){
		client.getListPerso();
	}
	
	@Test(expected = UnsupportedOperationException.class)
	public void testGetListeStatus(){
		client.getListStatut(FactoryStatut.create(STATUT_EN_TRAITEMENT));
	}
}
