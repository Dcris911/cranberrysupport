package com.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

import com.main.domaine.Client;
import com.main.domaine.Commentaire;

@RunWith(MockitoJUnitRunner.class)
public class TestCommentaire {
    
    Commentaire commentaire;
    Client client;
    
    @Before
    public void setUp() {
    	client = new Client("Bob", "Aime", "Le", "Fromage");
        commentaire = new Commentaire("BobAimeLeFromage", client);
    }
    String commentaire_str;
    
    @After
    public void tearDown() {
        commentaire = null;
        commentaire_str = null;
    }

    @Test
	public void testGetAuteur() {
    	assertEquals(client, commentaire.getAuteur());
	}

    @Test
	public void testGetComment() {
    	assertEquals("BobAimeLeFromage", commentaire.getComment());
	}

    @Test
	public void testToString() {
    	assertEquals("Le: BobAimeLeFromage" + "\n", commentaire.toString());
	}

    @Test
	public void testEqualsFonctionnel() {
    	assertTrue(commentaire.equals(new Commentaire("BobAimeLeFromage", client)));
	}

    @Test
	public void testEqualsNonFonctionnelPasMemeType() {
    	assertFalse(commentaire.equals(new Object()));
	}

    @Test
	public void testEqualsNonFonctionnelPasMemeCommentaire() {
    	assertFalse(commentaire.equals(new Commentaire("BobNAimePasLeFromage", client)));
	}

    @Test
	public void testEqualsNonFonctionnelPasMemeAuteur() {
    	Client client2 = new Client("", "", "", "");
    	assertFalse(commentaire.equals(new Commentaire("BobNAimePasLeFromage", client2)));
	}
}
