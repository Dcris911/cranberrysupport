package com.main.utilitaires;

import static com.main.utilitaires.Constante.TAG_STATUT_ABANDONNER;
import static com.main.utilitaires.Constante.TAG_STATUT_EN_TRAITEMENT;
import static com.main.utilitaires.Constante.TAG_STATUT_OUVERT;
import static com.main.utilitaires.Constante.TAG_STATUT_SUCCESS;

public class Constante {

	public static final String ROLE_CLIENT = "client";
	public static final String ROLE_TECHNICIEN = "technicien";
	
	public static final String STATUT_OUVERT = "ouvert";
	public static final String STATUT_EN_TRAITEMENT = "en traitement";
	public static final String STATUT_FINAL_ABANDON = "Abandon";
	public static final String STATUT_FINAL_SUCCESS = "Succes";

	
	public static final String TAG_STATUT_OUVERT = "Statut ouvert: ";
	public static final String TAG_STATUT_EN_TRAITEMENT = "Statut En traitement: ";
	public static final String TAG_STATUT_SUCCESS = "Statut succès: ";
	public static final String TAG_STATUT_ABANDONNER = "Statut abandonnée: ";
	
	public static String [] tags= {TAG_STATUT_OUVERT,TAG_STATUT_EN_TRAITEMENT,
			TAG_STATUT_SUCCESS,TAG_STATUT_ABANDONNER};
	
	public static final String CATEGORIE_POSTE_DE_TRAVAIL = "Poste de travail";
	public static final String CATEGORIE_SERVEUR = "Serveur";
	public static final String CATEGORIE_SERVICE_WEB = "Service web";
	public static final String CATEGORIE_COMPTE_USAGER = "Compte usager";
	public static final String CATEGORIE_AUTRE = "Autre";
	
	public static final String EXCEPTION_MESSAGE_NOT_SUPPORTED = "client";
	
	/** Frames */
	public static final String FRAME_NOM_USER = "Nom d'utilisateur:";
	public static final String FRAME_MDP = "Mot de passe:";
	public static final String FRAME_EXEMPLE_NOM_USER = "ex.: Isabeau Desrochers";
	public static final String FRAME_EXEMPLE_MDP = "ex.: MonMdp";
	public static final String FRAME_NO_REQUEST = "Vous n'avez pas de requete encore.";
	public static final String FRAME_NO_REQUEST_RIGHT_NOW = "Il n'y a pas de requetes pour le moment.";
	public static final String FRAME_OK = "OK";
	public static final String FRAME_ANNULER = "Annuler";
	public static final String FRAME_VOS_REQUETES = "Vos requetes:";
	public static final String FRAME_FAIRE_NEW_REQUETE = "Faire une nouvelle requete";
	public static final String FRAME_QUITTER = "Quitter";
	public static final String FRAME_AJOUT_COMMENTAIRE = "Ajouter un commentaire";
	public static final String FRAME_AJOUT_FICHIER = "Ajouter un fichier";
	public static final String FRAME_MODIFIER_REQUETE = "Modifier la requete selectionnee:";
	public static final String FRAME_VOIR_COMMENTAIRE = "Voir les commentaires";
	public static final String FRAME_VOIR_REQUETE = "Voir la requete";
	public static final String FRAME_DEMARRAGE = "demarrage";
	public static final String FRAME_DEMARRER_APPLICATION = "Demarrer l'application en tant que:";
	public static final String FRAME_UTILISATEUR = "Utilisateur";
	public static final String FRAME_TECHNICIEN = "Technicien";
	public static final String FRAME_MAUVAIS_CREDENTIAL = "Desole, votre nom d'utilisateur ou votre mot de passe est incorrect.";
	public static final String FRAME_RESTART = "Recommencer";
	public static final String FRAME_CONFIRMATION_AJOUT_REQUETE = "Fichier bien ajoute a� la requete";
	public static final String FRAME_NOUVELLE_REQUETE = "Nouvelle requete";
	public static final String FRAME_SUJET_REQUETE = "Sujet de la requete:";
	public static final String FRAME_DESCRIPTION = "Description:";
	public static final String FRAME_FICHIER = "Fichier:";
	public static final String FRAME_TELEVERSER = "Televerser un fichier";
	public static final String FRAME_CATEGORIE = "Categorie";
	public static final String FRAME_COMMENTAIRE = "Commentaires:";
	public static final String FRAME_TERMINER = "Terminer";
	public static final String FRAME_REVENIR = "Revenir";
	public static final String FRAME_NO_TECHNICIEN = "Il n'y a pas de technicien";
	public static final String FRAME_REQUETES_ASSIGNEE = "Les requetes qui vous sont assignees:";
	public static final String FRAME_REQUETE_SELECTIONNER = "Requete selectionnee:";
	public static final String FRAME_REQUEST_DISPO_NO_ASSIGN = "Requetes disponibles non-assignee:";
	public static final String FRAME_PRENDRE_REQUEST = "Prendre la requete selectionnee";
	public static final String FRAME_CREE_REQUETE = "Creer une nouvelle requete";
	public static final String FRAME_ASSIGNER_REQUETE_MEMBRE = "Assigner la requete a� ce membre";
	public static final String FRAME_AFFICHER_RAPPORT = "Afficher le rapport";
	public static final String FRAME_PAS_FICHIER = "(pas de fichier attache)";
}
