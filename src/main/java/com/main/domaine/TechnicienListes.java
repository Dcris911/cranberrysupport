package com.main.domaine;

import java.util.ArrayList;

public class TechnicienListes {
	private ArrayList<Requete> listeRequetesTech;
	//private ArrayList<Requete> listeRequetesFinies;
	//private ArrayList<Requete> listeRequetesEnCours;
	//private ArrayList<Requete> listeRequetesPerso;

	public TechnicienListes() {
		listeRequetesTech = new ArrayList<Requete>();
        //listeRequetesEnCours = new ArrayList<Requete>();
        //listeRequetesFinies = new ArrayList<Requete>();
        //listeRequetesPerso = new ArrayList<Requete>();
	}

	public ArrayList<Requete> getListeRequetesTech() {
		return listeRequetesTech;
	}

	public void setListeRequetesTech(ArrayList<Requete> listeRequetesTech) {
		this.listeRequetesTech = listeRequetesTech;
	}
/*
	public ArrayList<Requete> getListeRequetesFinies() {
		return listeRequetesFinies;
	}

	public void setListeRequetesFinies(ArrayList<Requete> listeRequetesFinies) {
		this.listeRequetesFinies = listeRequetesFinies;
	}

	public ArrayList<Requete> getListeRequetesEnCours() {
		return listeRequetesEnCours;
	}

	public void setListeRequetesEnCours(ArrayList<Requete> listeRequetesEnCours) {
		this.listeRequetesEnCours = listeRequetesEnCours;
	}

	public ArrayList<Requete> getListeRequetesPerso() {
		return listeRequetesPerso;
	}

	public void setListeRequetesPerso(ArrayList<Requete> listeRequetesPerso) {
		this.listeRequetesPerso = listeRequetesPerso;
	}
	*/
	public void ajoutRequetePerso(Requete nouvelleRequest) {
		listeRequetesTech.add(nouvelleRequest);
      //  listeRequetesPerso.add(nouvelleRequest);
	}
	
	public void ajouterRequeteAssignee(Requete assignee) {
        //listeRequetesEnCours.add(assignee);
        listeRequetesTech.add(assignee);
    }

    //Action sur les liste lorsqu'une requete est finalisée
    public void ajoutListRequetesFinies(Requete finie) {
        //listeRequetesFinies.add(finie);
        //listeRequetesEnCours.remove(finie);
    }
	
	
	
}