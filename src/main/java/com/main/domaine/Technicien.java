package com.main.domaine;


import java.util.ArrayList;
import com.main.domaine.Statut.Statut;
import static com.main.utilitaires.Constante.*;

public class Technicien extends Utilisateur {

    public TechnicienListes data;

    public Technicien(String prenom, String nom, String nomUtilisateur, String motDePasse) {
        super(prenom, nom, nomUtilisateur, motDePasse, ROLE_TECHNICIEN);
        data = new TechnicienListes();
    }

    @Override
    public void ajoutRequete(Requete req) {
    	data.ajoutRequetePerso(req);
    }
    
    @Override
    public ArrayList<Requete> getListPerso() {
        return data.getListeRequetesTech();
    }
    
  //Actions lorsqu'il se fait assigner une requete
    public void ajouterRequeteAssignee(Requete assignee) {
    	data.ajouterRequeteAssignee(assignee);
    }

    //Action sur les liste lorsqu'une requete est finalisée
    public void ajoutListRequetesFinies(Requete finie) {
    	data.ajoutListRequetesFinies(finie);
    }
    
    //Retourne la liste des requetes de ce statut
    @Override
    public ArrayList<Requete> getListStatut(Statut statut) {
        ArrayList<Requete> listeStatut = new ArrayList<Requete>();
        for (Requete requete : data.getListeRequetesTech()) {
            listeStatut = requete.ajouteSiStatutCorrect(statut.getStatut(), listeStatut);
         }
        return listeStatut;
    }
    
    @Override
    public ArrayList getListeRequetes() {
        return data.getListeRequetesTech();
    }

    //Retourne une string contenant pour ce technicien le nombre
    //de requetes en fonction de leur statut
    public String getRequeteParStatut() {
    	int [] status = {0,0,0,0};                
        for (Requete requete : data.getListeRequetesTech()) {
           requete.getStatistiqueRequete(status);
        }
        return statistiqueToString(tags, status);
    }
    
    public String statistiqueToString(String [] tags,int [] stats ) {
    	String text="";
    	for (int i = 0; i < stats.length; i++) {
			text = text + tags[i] + stats[i] + "\n";
		}
    	return text;
    }
    

	@Override
	public boolean equals(Object utilisateur) {
		if(utilisateur instanceof Technicien) {
			Technicien tech = (Technicien) utilisateur;
			if(nom.equals(tech.getNom())) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	 public String getSqlStatement() {
		 return "VALUES('"+ prenom +"','"+ nom+"','"+ nomUtilisateur+"','"+ motDePasse+"','technicien')";
	 }
	
}