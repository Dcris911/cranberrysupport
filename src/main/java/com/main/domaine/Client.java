package com.main.domaine;

import java.util.ArrayList;
import java.util.List;

import com.main.domaine.Statut.Statut;

import static com.main.utilitaires.Constante.ROLE_CLIENT;
import static com.main.utilitaires.Constante.EXCEPTION_MESSAGE_NOT_SUPPORTED;

public class Client extends Utilisateur {

    private ArrayList<Requete> listeRequetesClient;

    //Constructeur de client
    public Client(String prenom, String nom, String nomUtilisateur, String motDePasse) {
        super(prenom, nom, nomUtilisateur, motDePasse, ROLE_CLIENT);
        listeRequetesClient = new ArrayList<Requete>();
    }

    @Override
    public String getRole() {
        return ROLE_CLIENT;
    }

    @Override
    public void ajoutRequete(Requete nouvelle) {
        listeRequetesClient.add(nouvelle);
    }

    public Requete trouverRequeteParNumero(int numeroDeRequete) {
    	for(Requete requete : listeRequetesClient) {
    		if(requete.getNumero() == numeroDeRequete) {
    			return requete;
    		}
    	}
    	return null;
    }

    public List<Requete> trouverRequeteParSujet(String sujetRechercher) {
    	List<Requete> listeRequeteSujet = new ArrayList<>();
    	for(Requete requete : listeRequetesClient) {
    		if(requete.getSujet().equals(sujetRechercher)) {
    			listeRequeteSujet.add(requete);
    		}
    	}
    	return listeRequeteSujet;
    }
    
    @Override
    public ArrayList<Requete> getListeRequetes() {
        return listeRequetesClient;
    }

    @Override
    public ArrayList<Requete> getListPerso() {
        throw new UnsupportedOperationException(EXCEPTION_MESSAGE_NOT_SUPPORTED);
    }

    @Override
    public ArrayList<Requete> getListStatut(Statut statut) {
        throw new UnsupportedOperationException(EXCEPTION_MESSAGE_NOT_SUPPORTED);
    }

	@Override
	public boolean equals(Object utilisateur) {
		if(utilisateur instanceof Client) {
			Client client = (Client) utilisateur;
			if(nom.equals(client.getNom())) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	 public String getSqlStatement() {
		 return "VALUES('"+ prenom +"','"+ nom+"','"+ nomUtilisateur+"','"+ motDePasse+"','client')";
	 }
    
    
}