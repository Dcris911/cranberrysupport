package com.main.domaine.categorie;

import static com.main.utilitaires.Constante.CATEGORIE_POSTE_DE_TRAVAIL;
import static com.main.utilitaires.Constante.CATEGORIE_SERVEUR;
import static com.main.utilitaires.Constante.CATEGORIE_SERVICE_WEB;
import static com.main.utilitaires.Constante.CATEGORIE_COMPTE_USAGER;
import static com.main.utilitaires.Constante.CATEGORIE_AUTRE;;

public class Categorie {

	public static CategorieInterface create(String categorie) {
        switch (categorie) {
            case CATEGORIE_POSTE_DE_TRAVAIL: 	return new CategoriePosteDeTravail();
            case CATEGORIE_SERVEUR: 			return new CategorieServeur();
            case CATEGORIE_SERVICE_WEB: 		return new CategorieServiceWeb();
            case CATEGORIE_COMPTE_USAGER: 		return new CategorieCompteUsager();
            case CATEGORIE_AUTRE:				return new CategorieAutre();
            default: return null;
        }
    }
	
	public static String fromString(String categorie) {
        return create(categorie).getValue();
    }
}
