package com.main.domaine.categorie;

import static com.main.utilitaires.Constante.CATEGORIE_POSTE_DE_TRAVAIL;

public class CategoriePosteDeTravail implements CategorieInterface{

    @Override
    public String getValue() {
        return CATEGORIE_POSTE_DE_TRAVAIL;
    }
    
    @Override
	public boolean equals(Object obj) {
		return (obj instanceof CategoriePosteDeTravail);
	}
    
    @Override
    public String toString() {
        return getValue();
    }
}
