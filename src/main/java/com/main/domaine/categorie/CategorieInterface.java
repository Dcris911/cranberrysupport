package com.main.domaine.categorie;

public interface CategorieInterface {

	public String getValue();
	
	@Override
	public boolean equals(Object obj);
}
