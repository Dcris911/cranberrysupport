package com.main.domaine.categorie;

import static com.main.utilitaires.Constante.CATEGORIE_AUTRE;

public class CategorieAutre implements CategorieInterface{

    @Override
    public String getValue() {
        return CATEGORIE_AUTRE;
    }

	@Override
	public boolean equals(Object obj) {
		return (obj instanceof CategorieAutre);
	}
	
    @Override
    public String toString() {
        return getValue();
    }
}
