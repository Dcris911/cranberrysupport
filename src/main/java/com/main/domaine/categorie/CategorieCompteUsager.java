package com.main.domaine.categorie;

import static com.main.utilitaires.Constante.CATEGORIE_COMPTE_USAGER;

public class CategorieCompteUsager implements CategorieInterface {

    @Override
    public String getValue() {
        return CATEGORIE_COMPTE_USAGER;
    }
    
    @Override
	public boolean equals(Object obj) {
		return (obj instanceof CategorieCompteUsager);
	}
    
    @Override
    public String toString() {
        return getValue();
    }
}
