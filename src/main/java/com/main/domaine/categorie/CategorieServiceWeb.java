package com.main.domaine.categorie;

import static com.main.utilitaires.Constante.CATEGORIE_SERVICE_WEB;

public class CategorieServiceWeb implements CategorieInterface {

    @Override
    public String getValue() {
        return CATEGORIE_SERVICE_WEB;
    }
    
    @Override
	public boolean equals(Object obj) {
		return (obj instanceof CategorieServiceWeb);
	}
    
    @Override
    public String toString() {
        return getValue();
    }
}
