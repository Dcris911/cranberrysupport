package com.main.domaine.categorie;

import static com.main.utilitaires.Constante.CATEGORIE_SERVEUR;

public class CategorieServeur implements CategorieInterface {

    @Override
    public String getValue() {
        return CATEGORIE_SERVEUR;
    }
    
    @Override
	public boolean equals(Object obj) {
		return (obj instanceof CategorieServeur);
	}
    
    @Override
    public String toString() {
        return getValue();
    }
}
