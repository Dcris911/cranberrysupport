package com.main.domaine;

import java.util.ArrayList;

import com.main.domaine.Statut.Statut;

public interface UtilisateurListe {

    public abstract ArrayList<Requete> getListStatut(Statut statut);

    public abstract ArrayList<Requete> getListeRequetes();

    public abstract void ajoutRequete(Requete nouvelle);

    public abstract ArrayList<Requete> getListPerso();

}