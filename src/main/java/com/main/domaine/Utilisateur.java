package com.main.domaine;


import java.util.ArrayList;

public abstract class Utilisateur implements UtilisateurListe {

    protected String nom;
    protected String prenom;
    protected String nomUtilisateur;
    protected String motDePasse;
    protected Integer telephone;
    protected String mail;
    protected String bureau;
    public String role;
    public ArrayList<String> info;

    //Constructeur complet

    public Utilisateur(String prenom, String nom, String nomUtilisateur, String motDePasse, String role) {
        this.prenom = prenom;
        this.nom = nom;
        this.nomUtilisateur = nomUtilisateur;
        this.motDePasse = motDePasse;
        this.role = role;
        this.info = new ArrayList<String>();
        this.telephone = new Integer(0);
    }

    public String getNom() {
        return nom;
    }

    public String getNomUtil() {
        return nomUtilisateur;
    }
    
    public String getRole() {
        return role;
    }
    
    public ArrayList<String> fetchInfos() {
        info.add(this.nom);
        info.add(prenom);
        info.add(nomUtilisateur);
        info.add(telephone.toString());
        info.add(mail);
        info.add(bureau);
        info.add(role);
        return info;
    }
    
    //Verifie les informations à la connexion
    public boolean login(String util, String motDePasse) {
        if (nomUtilisateur.equalsIgnoreCase(util) && this.motDePasse.equals(motDePasse)) {
            return true;
        }
        return false;
    }
    
    @Override
	public String toString() {
		return "Utilisateur [nom=" + nom + ", prenom=" + prenom + ", nomUtilisateur=" + nomUtilisateur + ", motDePasse="
				+ motDePasse + ", telephone=" + telephone + ", mail=" + mail + ", bureau=" + bureau + ", role=" + role
				+ ", info=" + info + "]";
	}

	public abstract String getSqlStatement();
}