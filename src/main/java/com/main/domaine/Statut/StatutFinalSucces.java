package com.main.domaine.Statut;

import static com.main.utilitaires.Constante.*;

public class StatutFinalSucces implements Statut{

    @Override
    public String getStatut() {
        return STATUT_FINAL_SUCCESS;
    }

	@Override
	public boolean equals(Object obj) {
		return (obj instanceof StatutFinalSucces);
	}
	
	@Override
	 public boolean isStatusPareil(String statut) {
		return statut.equals(STATUT_FINAL_SUCCESS);
	}
	
	@Override
	public int [] remplirTableauRequeteParStatut(int [] statusTotal) {
		statusTotal[2]++;
		return statusTotal;
	}
	
    @Override
    public String toString() {
        return getStatut();
    }
}
