package com.main.domaine.Statut;

import static com.main.utilitaires.Constante.*;

public class StatutEnTraitement implements Statut{

    @Override
    public String getStatut() {
        return STATUT_EN_TRAITEMENT;
    }
    
	@Override
	public boolean equals(Object obj) {
		return (obj instanceof StatutEnTraitement);
	}
	
	@Override
	 public boolean isStatusPareil(String statut) {
		return statut.equals(STATUT_EN_TRAITEMENT);
	}
	
	@Override
	public int [] remplirTableauRequeteParStatut(int [] statusTotal) {
		statusTotal[1]++;
		return statusTotal;
	}

    @Override
    public String toString() {
        return getStatut();
    }
	
}
