package com.main.domaine.Statut;

public interface Statut {
    
    public String getStatut();
    
    public int [] remplirTableauRequeteParStatut(int [] statusTotal);
    
    public boolean isStatusPareil(String statut);

	@Override
	boolean equals(Object obj);
	
}
