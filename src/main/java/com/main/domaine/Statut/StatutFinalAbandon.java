package com.main.domaine.Statut;

import static com.main.utilitaires.Constante.*;

public class StatutFinalAbandon implements Statut{
    
    @Override
    public String getStatut() {
        return STATUT_FINAL_ABANDON;
    }

	@Override
	public boolean equals(Object obj) {
		return (obj instanceof StatutFinalAbandon);
	}
	
	@Override
	 public boolean isStatusPareil(String statut) {
		return statut.equals(STATUT_FINAL_ABANDON);
	}
	
	@Override
	public int [] remplirTableauRequeteParStatut(int [] statusTotal) {
		statusTotal[3]++;
		return statusTotal;
	}
	
    @Override
    public String toString() {
        return getStatut();
    }
    
}
