package com.main.domaine.Statut;

import static com.main.utilitaires.Constante.*;

public class StatutOuvert implements Statut{

    @Override
    public String getStatut() {
        return STATUT_OUVERT;
    }

	@Override
	public boolean equals(Object obj) {
		return (obj instanceof StatutOuvert);
	}
	
	@Override
	 public boolean isStatusPareil(String statut) {
		return statut.equals(STATUT_OUVERT);
	}
	
	@Override
	public int [] remplirTableauRequeteParStatut(int [] statusTotal) {
		statusTotal[0]++;
		return statusTotal;
	}
	
    @Override
    public String toString() {
        return getStatut();
    }
}
