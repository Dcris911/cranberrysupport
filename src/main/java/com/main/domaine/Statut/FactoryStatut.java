package com.main.domaine.Statut;

import com.main.utilitaires.Constante;

public class FactoryStatut {
    
    public static Statut create(String statut) {
        switch (statut) {
            case Constante.STATUT_OUVERT: return new StatutOuvert();
            
            case Constante.STATUT_EN_TRAITEMENT: return new StatutEnTraitement();
        
            case Constante.STATUT_FINAL_ABANDON: return new StatutFinalAbandon();
        
            case Constante.STATUT_FINAL_SUCCESS: return new StatutFinalSucces();
            
            default: return null; 
        }
    }
    
    public static String fromString(String statut) {
        return create(statut).getStatut();
    }
}
