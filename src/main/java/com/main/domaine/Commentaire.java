package com.main.domaine;

public class Commentaire {

    protected String commentaire;
    protected Utilisateur auteur;
    
    public Commentaire(String commentaire, Utilisateur auteur) {
        this.commentaire = commentaire;
        this.auteur = auteur;
    }

    public Utilisateur getAuteur() {
        return auteur;
    }

    public String getComment() {
        return commentaire;
    }

    public String toString() {
        return getAuteur().getNomUtil() + ": " + getComment() + "\n";
    }

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Commentaire) {
			Commentaire comment = (Commentaire) obj;
			return (getComment() == comment.getComment() 
					&& getAuteur().getNom() == comment.getAuteur().getNom());
		}
		return false;
	}
    
}