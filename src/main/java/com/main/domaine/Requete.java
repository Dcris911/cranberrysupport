package com.main.domaine;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.main.domaine.categorie.CategorieInterface;
import com.main.database.fichiertxt.BanqueRequetesTxt;
import com.main.domaine.Statut.FactoryStatut;
import com.main.domaine.Statut.Statut;

import static com.main.utilitaires.Constante.*;

public class Requete {

    private String sujet = "";
    private String description = "";
    private File fichier;
    private Integer numero;
    private Utilisateur client;
    private Technicien technicien;
    private Statut statut;
    private CategorieInterface cat;
    private ArrayList<Commentaire> listeCommentaires;

    
    //Petit constructeur
    public Requete(String sujet, String description, Client client, CategorieInterface cat) throws FileNotFoundException, IOException {
        this.sujet = sujet;
        this.description = description;
        this.client = client;

        numero = BanqueRequetesTxt.getRequestInstance().incrementeNo();
        this.statut = FactoryStatut.create(STATUT_OUVERT);
        this.cat = cat;
        listeCommentaires = new ArrayList<Commentaire>();
    }

    //Constructeur complet
    public Requete(String sujet, String description, Utilisateur client, CategorieInterface categorie) throws FileNotFoundException, FileNotFoundException, IOException {
        this.client = client;
        this.sujet = sujet;
        this.description = description;
        if (client.getRole().equals(ROLE_CLIENT)) {
            this.client = (Client) client;
        } else {
            this.technicien = (Technicien) client;
        }

        this.numero = BanqueRequetesTxt.getRequestInstance().incrementeNo();
        this.statut = FactoryStatut.create(STATUT_OUVERT);
        this.cat = categorie;
        this.listeCommentaires = new ArrayList<Commentaire>();
    }
    
    public String getSujet() {
        return sujet;
    }
    public void setSujet(String nouveau) {
        sujet = nouveau;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String nouvelle) {
        description = nouvelle;
    }

    public CategorieInterface getCategorie() {
        return cat;
    }

    public void setCategorie(CategorieInterface choix) {
        cat = choix;
    }
    
    public File getFichier() {
        return fichier;
    }

    public void setFichier(File file) {
        fichier = file;
    }

    public void uploadFichier() {
    }
    
    public Integer getNumero() {
        return numero;
    }
    public void setNumero(int numero) {
    	this.numero = numero;
    } 

    public Utilisateur getClient() {
        return client;
    }

    public Technicien getTech() {
        return technicien;
    }

    //Il n'y a pas de Technicien au départ à moins qu'il la crée lui-meme
    //A un certain point il faut donc désigner un technicien
    public void setTech(Utilisateur tech) {
        if (tech != null) {
            if (tech.getRole().equals(ROLE_TECHNICIEN)) {
                this.technicien = (Technicien) tech;
                technicien.ajouterRequeteAssignee(this);
            }
        }
    }

    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut newstat) {
        statut = newstat;
    }

    public ArrayList<Commentaire> getComments() {
        return listeCommentaires;
    }

    public void finaliser(Statut fin) {
        this.setStatut(fin);
        this.technicien.ajoutListRequetesFinies(this);
    }

    //Ajout de commentaire dans la liste des commentaires
    public void addCommentaire(String aDire, Utilisateur toi) {
        Commentaire suivant = new Commentaire(aDire, toi);
        listeCommentaires.add(suivant);
    }
    
	public int [] getStatistiqueRequete(int [] statusTotal) {
		return statut.remplirTableauRequeteParStatut(statusTotal);
	}
    
	public ArrayList<Requete> ajouteSiStatutCorrect(String statusType, ArrayList<Requete> list) {
		if(this.statut.isStatusPareil(statusType)){
			list.add(this);
		}
		return list;
	}
	/**
	 * private String sujet = "";
    private String description = "";
    private File fichier;
    private Integer numero;
    private Utilisateur client;
    private Technicien technicien;
    private Statut statut;
    private CategorieInterface cat;
    private ArrayList<Commentaire> listeCommentaires;
	 * @return
	 */
	 public String getSqlStatement() {
		 return "VALUES('"+ sujet +"','"+ description+"','"+ numero+"','"+ client.nomUtilisateur
				 +"','"+ technicien.nomUtilisateur+"','"+ statut.getStatut()+"','"+ cat.getValue()+"',?)";
	 }
    
}