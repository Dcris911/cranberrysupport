package com.main.database;

import static com.main.utilitaires.Constante.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.main.domaine.Client;
import com.main.domaine.Requete;
import com.main.domaine.Statut.Statut;
import com.main.domaine.Statut.StatutEnTraitement;
import com.main.domaine.Statut.StatutFinalAbandon;
import com.main.domaine.Statut.StatutFinalSucces;
import com.main.domaine.Statut.StatutOuvert;
import com.main.domaine.categorie.Categorie;


/**
 * Classe pour garder les requetes en SQLite
 * PAS UTILISER (PAS FINI)
 *
 */
public class RepoRequete {
	// SQLite connection string
		private String url = "jdbc:sqlite:dat/data.db";
		private String sqlCreatingTable = "CREATE TABLE IF NOT EXISTS requete (  sujet text NOT NULL,	"
				+ "description text, fichier blob ,numero number primary Key,nomUtilisateur text NOT NULL,"
				+ "nomtechnicien text NOT NULL, statut text NOT NULL, categorie text NOT NULL,"
				+ "FOREIGN KEY(nomUtilisateur) REFERENCES utilisateur(nomUtilisateur),"
				+ "FOREIGN KEY(nomtechnicien) REFERENCES utilisateur(nomUtilisateur));";
		private String sqlInsert = "INSERT INTO requete (sujet, description, numero, nomUtilisateur,"
				+ " nomtechnicien, statut,categorie,fichier) ";
		private String sqlDropTable = "DROP TABLE IF EXISTS requete";
		RepoUtilisateur repoUser;
		
		public RepoRequete() {
			initDataBase();
		}
		
		public void modeTest(RepoUtilisateur repo) {
			this.url = "jdbc:sqlite:dat/dataTest.db";
			repoUser = repo;
			repo.modeTest();
			
			initDataBase();
		}
		
		public void dropTable() {
			repoUser = new RepoUtilisateur();
			repoUser.dropTable();
			try (Connection connect = DriverManager.getConnection(url); Statement statement = connect.createStatement()) {
				if (connect != null) {
					statement.execute(sqlDropTable);
				}
			} catch (SQLException e) {
				System.out.println(e.getMessage());

			}
		}

		public void initDataBase() {
			try (Connection connect = DriverManager.getConnection(url); Statement statement = connect.createStatement()) {
				if (connect != null) {
					DatabaseMetaData meta = connect.getMetaData();
					statement.execute(sqlCreatingTable);
				}
			} catch (SQLException e) {
				System.out.println(e.getMessage());

			}
		}
		
		private Connection connect() {
			Connection conn = null;
			try {
				conn = DriverManager.getConnection(url);
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			return conn;
		}

		public void addRequeteToDataBase(Requete requete) {
			repoUser.addUserToDataBase(requete.getClient());
			repoUser.addUserToDataBase(requete.getTech());
			try (Connection conn = connect()) {
				PreparedStatement statement = conn.prepareStatement(sqlInsert+requete.getSqlStatement());
				statement.setObject(1, requete.getFichier());
				statement.executeUpdate();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		
		private Requete getSingleRequestFromDataBase(String sql) {
			Requete actuel= null;
			repoUser = new RepoUtilisateur();
			try (Connection connection = connect();
					Statement statement = connection.createStatement();
					ResultSet resultat = statement.executeQuery(sql)) {
				
				while (resultat.next()) {
					actuel = new Requete(resultat.getString("sujet"), resultat.getString("description"), 
							((Client)repoUser.findUserByUserName(resultat.getString("nomUtilisateur"))), 
							Categorie.create(resultat.getString("categorie")));
					actuel.setNumero(resultat.getInt("numero"));
					actuel.setStatut(dispatchStatus(resultat.getString("statut")));
					actuel.setTech(repoUser.findUserByUserName(resultat.getString("nomtechnicien")));

				}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return actuel;
		}
		
		private ArrayList<Requete>  getListRequeteFromDataBase(String sql) {
			ArrayList<Requete> list = new ArrayList<>();
			RepoUtilisateur repoUser = new RepoUtilisateur();
			
			try (Connection connection = connect();
					Statement statement = connection.createStatement();
					ResultSet resultat = statement.executeQuery(sql)) {
					
				while (resultat.next()) {
					try {
						Requete actuel = new Requete(resultat.getString("sujet"), resultat.getString("description"), 
								((Client)repoUser.findUserByUserName(resultat.getString("nomUtilisateur"))), 
								Categorie.create(resultat.getString("categorie")));
						actuel.setNumero(resultat.getInt("numero"));
						actuel.setStatut(dispatchStatus(resultat.getString("statut")));
						actuel.setTech(repoUser.findUserByUserName(resultat.getString("nomtechnicien")));
						
						list.add( actuel);
						
					} catch (IOException e) {
						e.printStackTrace();
					}

				}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			return list;
		}
		
		
		public Statut dispatchStatus(String statut){
			if(statut.equals(TAG_STATUT_OUVERT)) {
				return new StatutOuvert();
			}else if(statut.equals(TAG_STATUT_EN_TRAITEMENT)) {
				return new StatutEnTraitement();
			}else if(statut.equals(TAG_STATUT_SUCCESS)) {
				return new StatutFinalSucces();
			}else if(statut.equals(TAG_STATUT_ABANDONNER)) {
				return new StatutFinalAbandon();
			}
			return null;
		}
		
		public Requete findRequeteParNumero(int numero) {
			String sql = "SELECT * FROM requete where numero = '"+numero+"'";
			
			return getSingleRequestFromDataBase(sql);
		}

		public void addAllRequete(ArrayList<Requete> requete) {
			for (Requete actuel : requete) {
				addRequeteToDataBase(actuel);
			}
		}
	
		public ArrayList<Requete> findByClient(String userId) {
			String sql = "SELECT * FROM requete where nomUtilisateur='"+userId+"'";
			return getListRequeteFromDataBase( sql);
		}
		
		public ArrayList<Requete> findByTech(String userId) {
			String sql = "SELECT * FROM requete where nomtechnicien='"+userId+"'";
			return getListRequeteFromDataBase( sql);
		}
		
		public ArrayList<Requete> findByStatut(String statut) {
			String sql = "SELECT * FROM requete where statut='"+statut+"'";
			return getListRequeteFromDataBase( sql);
		}
		
		public ArrayList<Requete> findByCategorie(String categorie) {
			String sql = "SELECT * FROM requete where categorie='"+categorie+"'";
			return getListRequeteFromDataBase( sql);
		}

		public ArrayList<Requete> findAll() {
			String sql = "SELECT * FROM requete";
			return getListRequeteFromDataBase( sql);
		}
		
}
