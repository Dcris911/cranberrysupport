package com.main.database;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import com.main.domaine.Requete;
import com.main.domaine.Utilisateur;
import com.main.domaine.Statut.Statut;
import com.main.domaine.categorie.CategorieInterface;

public abstract class BanqueRequetes {

    protected Integer numero = -1;
    protected ArrayList<Requete> listeRequetes;
    
    public ArrayList<Requete> getListRequetes() {
        return listeRequetes;
    }
    
    public Integer getNo() {
        return this.numero;
    }
    
    public Integer incrementeNo() {
        return ++numero;
    }
    
    public Requete returnLast() {
        return listeRequetes.get(listeRequetes.size() - 1);
    }
    
    public void newRequete(String sujet, String desrcrip, Utilisateur client, CategorieInterface cat)
            throws FileNotFoundException, IOException {
    	
        Requete nouvelle = new Requete(sujet, desrcrip, client, cat);
        listeRequetes.add(nouvelle);
        client.ajoutRequete(nouvelle);
    }
    
    public ArrayList<Requete> getListRequetesByStatut(Statut statut) {
        ArrayList<Requete> statuts = new ArrayList<Requete>();
        for (Requete requete : listeRequetes) {
            if (requete.getStatut().equals(statut)) {
                statuts.add(requete);
            }
        }
        return statuts;
    }
    
    public abstract void chargerRequetes() throws FileNotFoundException, IOException;
    public abstract void saveRequetes() throws IOException;
}
