package com.main.database.fichiertxt;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import com.main.database.BanqueUtilisateurs;
import com.main.database.RepoUtilisateur;
import com.main.domaine.Client;
import com.main.domaine.Technicien;
import com.main.domaine.Utilisateur;
import com.main.domaine.UtilisateurListe;

import static com.main.utilitaires.Constante.ROLE_CLIENT;

public class BanqueUtilisateursTxt extends BanqueUtilisateurs{

    private static BanqueUtilisateursTxt ListeUsers = null;

    //Constructeur de la Banque
    public BanqueUtilisateursTxt() throws FileNotFoundException {

    }

    //Il s'agit d'un singleton, cette mehode retourne la seule
    //instance de la classe
    public static BanqueUtilisateursTxt getUserInstance() throws FileNotFoundException {
        if (ListeUsers != null) {
            return ListeUsers;
        } else {
            ListeUsers = new BanqueUtilisateursTxt();
            ListeUsers.chargerUtilisateur();
            return ListeUsers;
        }
    }
    
    //Remplie la liste au demarrage
    public void chargerUtilisateur() throws FileNotFoundException {
        
    	super.repo.initDataBase();
    }

	@Override
	public void saveUtilisateur() {		
	}

}