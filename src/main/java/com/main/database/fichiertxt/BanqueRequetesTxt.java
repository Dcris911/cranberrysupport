package com.main.database.fichiertxt;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import com.main.database.BanqueRequetes;
import com.main.database.RepoRequete;
import com.main.database.RepoUtilisateur;
import com.main.domaine.Requete;
import com.main.domaine.Technicien;
import com.main.domaine.Utilisateur;
import com.main.domaine.Statut.FactoryStatut;
import com.main.domaine.Statut.Statut;
import com.main.domaine.categorie.CategorieInterface;
import com.main.domaine.categorie.Categorie;

public class BanqueRequetesTxt extends BanqueRequetes {

    private static BanqueRequetesTxt instance = null;
    
    //rendu public pour les test
    public BanqueRequetesTxt() throws FileNotFoundException, IOException {
        listeRequetes = new ArrayList<Requete>(100);

    }

    //Il s'agit d'un singleton
    public static BanqueRequetesTxt getRequestInstance() throws FileNotFoundException, IOException {
        if (instance != null) {
            return instance;
        } else {
            instance = new BanqueRequetesTxt();
            instance.chargerRequetes();
            return instance;

        }
    }

    //Méthode du remplissage de la liste de requête à l'ouverture
    public void chargerRequetes() throws FileNotFoundException, IOException {
        File f = new File("dat/BanqueRequetes.txt");
        if (f.exists()) {
            Scanner banqueRIn = new Scanner(new FileReader(f));
            while (banqueRIn.hasNextLine()) {
                String s = banqueRIn.nextLine();
                String[] ss = s.split(";~", -1);
                this.newRequeteString(ss[0], ss[1], ss[2], ss[3], ss[4], ss[5], ss[6], ss[7]);
            }
        }
    }

    //Methode de cretion d'une requete a� partir du fichier texte ou elle
    //est sauvegarde
    private void newRequeteString(String sujet, String descrip, String client,
            String categorie, String statut, String path, String tech, String comm)
            throws FileNotFoundException, IOException {
    	
    	
        Requete nouvelle = ajoutRequeteListeRequeteEtClient(sujet, descrip, client, categorie, statut);
        
        File tempo = new File(path);
        nouvelle.setFichier(tempo);
        nouvelle.setTech(BanqueUtilisateursTxt.getUserInstance().chercherUtilisateurParNomUtil(tech));

        if (!comm.equals("")) {
            String[] comms = comm.split("/%");
            for (int i = 0; i < comms.length; i++) {
                String[] ceCommentaire = comms[i].split("&#");
                nouvelle.addCommentaire(ceCommentaire[0],
                        BanqueUtilisateursTxt.getUserInstance().chercherUtilisateurParNomUtil(ceCommentaire[1]));
            }
        }
    }

	private Requete ajoutRequeteListeRequeteEtClient(String sujet, String descrip, String client, String categorie,
			String statut) throws FileNotFoundException, IOException {
		Requete nouvelle = creationRequete(sujet, descrip, client, categorie, statut);
        listeRequetes.add(nouvelle);
        nouvelle.getClient().ajoutRequete(nouvelle);
		return nouvelle;
	}

	private Requete creationRequete(String sujet, String descrip, String client, String categorie, String statut)
			throws FileNotFoundException, IOException {
		Requete nouvelle = new Requete(sujet, descrip,
                BanqueUtilisateursTxt.getUserInstance().chercherUtilisateurParNomUtil(client),
                Categorie.create(categorie));
        nouvelle.setStatut(FactoryStatut.create(statut));
		return nouvelle;
	}
    
    //Méthode qui enregistre toutes les requêtes à la fermeture de l'app
    public void saveRequetes() throws IOException {
        FileWriter sauvegarde = new FileWriter("dat/BanqueRequetes.txt");
        BufferedWriter saving = new BufferedWriter(sauvegarde);
        for (Requete requete : listeRequetes) {
            String comments = "";
            if (requete.getComments() != null) {
                for (int j = 0; j < requete.getComments().size(); j++) {
                    comments += requete.getComments().get(j).getComment();
                    comments += "&#" + requete.getComments().get(j).getAuteur().getNomUtil();
                    if (requete.getComments().size() - 1 == j) {
                        comments += "";
                    } else {
                        comments += "/%";
                    }
                }
            }
            final String sep = ";~";
            String path = "";
            String tech = "";
            String categorie = requete.getCategorie().getValue();
            if (requete.getTech() != null) {
                tech = requete.getTech().getNomUtil();
            }
            if (requete.getFichier() != null) {
                path = requete.getFichier().getPath();
            }
            String s = requete.getSujet() + sep
                    + requete.getDescription() + sep
                    + requete.getClient().getNomUtil() + sep
                    + categorie + sep
                    + requete.getStatut().getStatut() + sep
                    + path + sep + tech + sep 
                    + comments + "\n";

            saving.write(s);
        }
        //Ferme le stream
        saving.close();

    }
}
