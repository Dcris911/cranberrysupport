package com.main.database;

import java.sql.Connection;

import static com.main.utilitaires.Constante.*;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.sqlite.SQLiteJDBCLoader;

import com.main.domaine.Client;
import com.main.domaine.Technicien;
import com.main.domaine.Utilisateur;

public class RepoUtilisateur {
	// SQLite connection string
	private String url = "jdbc:sqlite:dat/data.db";
	private String sqlCreatingTable = "CREATE TABLE IF NOT EXISTS utilisateur (  prenom text NOT NULL,	"
			+ "nom text NOT NULL primary Key, nomUtilisateur text NOT NULL,motDePasse text NOT NULL,categorie text NOT NULL);";
	private String sqlInsert = "INSERT INTO utilisateur (prenom, nom, nomUtilisateur,motDePasse,categorie) ";
	private String sqlDropTable = "DROP TABLE IF EXISTS utilisateur";
	
	public RepoUtilisateur() {
		initDataBase();
	}
	
	public void modeTest() {
		this.url = "jdbc:sqlite:dat/dataTest.db";
		initDataBase();
	}
	
	public void dropTable() {
		try (Connection connect = DriverManager.getConnection(url); Statement statement = connect.createStatement()) {
			if (connect != null) {
				statement.execute(sqlDropTable);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		}
	}
	
	public void initDataBase() {
		try (Connection connect = DriverManager.getConnection(url); Statement statement = connect.createStatement()) {
			if (connect != null) {
				DatabaseMetaData meta = connect.getMetaData();
				statement.execute(sqlCreatingTable);
				peuplementInit();
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		}
	}
	
	public void peuplementInit() {
		addUserToDataBase(new Client("Roger", "Danfousse", "rogerdanfousse", "jesuiscool"));
		addUserToDataBase(new Client("Martine", "Jodoin", "martinejodoin", "moiaussi"));
		addUserToDataBase(new Client("Pauline", "St-Onge", "popoline", "monchatkiki"));
		addUserToDataBase(new Technicien("Richard", "Chabot", "chabotr", "chabotte"));
		addUserToDataBase(new Technicien("Louise", "Boisvert", "techguest", "password"));
	}

	private Connection connect() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return conn;
	}

	public boolean addUserToDataBase(Utilisateur user) {
		try (Connection conn = connect()) {
			Statement statement = conn.createStatement();
			if(findUserByName(user.getNom())==null && findUserByUserName(user.getNomUtil())==null) {
				statement.execute(sqlInsert + user.getSqlStatement());
			}else {
				return false;
			}
		} catch (SQLException e) {
			return false;
		}
		return true;
	}
	
	private Utilisateur getSingleUserFromDataBase(String sql) {
		Utilisateur result = null;

		try (Connection connection = connect();
				Statement statement = connection.createStatement();
				ResultSet resultat = statement.executeQuery(sql)) {
			
			while (resultat.next()) {
				if(resultat.getString("categorie").equals(ROLE_CLIENT)) {
					result = (new Client(resultat.getString("prenom"), resultat.getString("nom"),
							resultat.getString("nomUtilisateur") ,resultat.getString("motDePasse") ));
				}else {
					result = (new Technicien(resultat.getString("prenom"), resultat.getString("nom"),
							resultat.getString("nomUtilisateur") ,resultat.getString("motDePasse") ));
				}

			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return result;
	}
	
	private ArrayList<Utilisateur>  getListUserFromDataBase(String sql) {
		ArrayList<Utilisateur> list = new ArrayList<>();

		try (Connection connection = connect();
				Statement statement = connection.createStatement();
				ResultSet resultat = statement.executeQuery(sql)) {
				
			while (resultat.next()) {
				if(resultat.getString("categorie").equals("client")) {
					list.add(new Client(resultat.getString("prenom"), resultat.getString("nom"),
							resultat.getString("nomUtilisateur") ,resultat.getString("motDePasse") ));
				}else {
					list.add(new Technicien(resultat.getString("prenom"), resultat.getString("nom"),
							resultat.getString("nomUtilisateur") ,resultat.getString("motDePasse") ));
				}

			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return list;
	}
	
	public Utilisateur findUserByUserName(String userName) {
		String sql = "SELECT * FROM utilisateur where nomUtilisateur = '"+userName+"'";
		
		return getSingleUserFromDataBase(sql);
	}

	public Utilisateur findUserByName(String name) {
		String sql = "SELECT * FROM utilisateur where nom = '"+name+"'";
		return getSingleUserFromDataBase(sql);
	}
	
	public ArrayList<Utilisateur> findByRole(String role) {
		String sql = "SELECT * FROM utilisateur where categorie='"+role+"'";
		return getListUserFromDataBase( sql);
	}

	public ArrayList<Utilisateur> findAll() {
		String sql = "SELECT * FROM utilisateur";
		return getListUserFromDataBase( sql);
	}
	
	public void main(String[] args) {
		initDataBase();
		System.out.println(findAll());
	}

}
