package com.main;


/**
 *
 * @author Anonyme
 */
import java.awt.*;
import java.io.FileNotFoundException;

import java.io.IOException;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.main.database.fichiertxt.BanqueRequetesTxt;
import com.main.frame.CranberryFrame;

public class Main {

    
    public static void main(String args[]) throws FileNotFoundException, IOException {

        BanqueRequetesTxt.getRequestInstance();

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(CranberryFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } 
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new CranberryFrame().setVisible(true);
            }
        });



        //S'assure de sauvegarder avant de quitter l'application
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    BanqueRequetesTxt.getRequestInstance().saveRequetes();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }));
    }
}
