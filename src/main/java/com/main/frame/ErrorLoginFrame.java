package com.main.frame;

import static com.main.utilitaires.Constante.*;

/*
 * ErrorLoginFrame.java
 *
 */

/**
 *
 * @author Anonyme
 */
public class ErrorLoginFrame extends javax.swing.JFrame {

    /** Creates new form ErrorLoginFrame */
    public ErrorLoginFrame() {
        initComponents();
        this.setVisible(true);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dslLbl = new javax.swing.JLabel();
        revenirLbl = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        dslLbl.setText(FRAME_MAUVAIS_CREDENTIAL);

        revenirLbl.setText(FRAME_RESTART);
        revenirLbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                revenirLblMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(81, Short.MAX_VALUE)
                .addComponent(dslLbl)
                .addGap(72, 72, 72))
            .addGroup(layout.createSequentialGroup()
                .addGap(184, 184, 184)
                .addComponent(revenirLbl)
                .addContainerGap(194, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(dslLbl)
                .addGap(18, 18, 18)
                .addComponent(revenirLbl)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void revenirLblMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_revenirLblMouseClicked
        this.setVisible(false);
        ClientFrame nouvelEssai = new ClientFrame();
    }//GEN-LAST:event_revenirLblMouseClicked

    /**
    * @param args the command line arguments
    */


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel dslLbl;
    private javax.swing.JButton revenirLbl;
    // End of variables declaration//GEN-END:variables

}
